
cackle_widget = window.cackle_widget || [];
cackle_widget.push({widget: 'Comment', id: Drupal.settings.cackle.cackle_mc_site, channel: Drupal.settings.cackle.node_id, ssoAuth: Drupal.settings.cackle.sso });
document.getElementById('mc-container').innerHTML = '';
(function() {
    var mc = document.createElement('script');
    mc.type = 'text/javascript';
    mc.async = true;
    mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
})();

