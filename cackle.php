<?php
/**
 * @file
 * Cackle social comments.
 */

define('CACKLE_TIMER', 500);
class Cackle {
  /**
   * Initializing comment sync.
   */
  public function __construct() {
    if ($this->timeIsOver(CACKLE_TIMER)) {
      $this->init(variable_get('cackle_mc_site', 0), variable_get('cackle_site_api_key', NULL), variable_get('cackle_account_api_key', NULL));
    }
  }

  /**
   * Timer function.
   */
  public function timeIsOver($cron_time) {
    $get_last_time = variable_get('cackle_last_time', 1);
    $now = time();
    if ($get_last_time == NULL) {
      variable_set('cackle_last_time', $now);
      return time();
    }
    else {
      if ($get_last_time + $cron_time > $now) {
        return FALSE;
      }
      if ($get_last_time + $cron_time < $now) {
        variable_set('cackle_last_time', $now);
        return $cron_time;
      }
    }
  }

  /**
   * Get request function.
   */

    function has_next ($size_comments, $size_pagination = 100) {
        return $size_comments == $size_pagination;
    }
    function push_next_comments($id,$apikey,$sitekey,$comment_last_modified, $size_comments){
        $i = 1;
        while($this->has_next($size_comments)){

            $response = $this->getComments($id,$apikey,$sitekey,$comment_last_modified,$i) ;

            $size_comments = $this->push_comments($response); // get comment from array and insert it to wp db
        }
    }


    public function to_i($number_to_format){
    return number_format($number_to_format, 0, '', '');
  }

  /**
   * Comment sync function.
   */
  public function init($id,$apikey,$sitekey) {

    $comment_last_modified =  variable_get('cackle_last_modified',0);
    $response = $this->get_comments($id,$apikey,$sitekey,$this->to_i($comment_last_modified));
    //get comments from Cackle Api for sync
    if ($response==NULL){
      return false;
    }

    $size_comments = $this->pushComments($response); // get comment from array and insert it to wp db, and return size

    if ($this->has_next($size_comments)) {
        $this->push_next_comments($id,$apikey,$sitekey,$comment_last_modified, $size_comments);
    }



    return "success";
  }

  public function get_comments($id,$sitekey,$apikey,$comment_last_modified, $cackle_page = 0){
    $this->get_url = "http://cackle.me/api/3.0/comment/list.json?id=$id&accountApiKey=$apikey&siteApiKey=$sitekey";
    $host = $this->get_url . "&modified=" . $comment_last_modified . "&page=" . $cackle_page . "&size=100";
    $result = @file_get_contents($host);
     // var_dump($host);die();
    return $result;

  }


  /**
   * Json decodes function.
   */
  public function cackle_json_decodes($response){
    $obj = json_decode($response,true);
    return $obj;
  }


  /**
   * Insert each comment to database with parents.
   */
  public function insertComment($comment, $status) {
    if (!is_numeric($comment['chan']['channel'])) {
      return;
    }
    else {
      $url = $comment['chan']['channel'];
    }
    if ($comment['author'] != NULL) {
      $name = (isset($comment['author']['name'])) ? $comment['author']['name'] : "";
      $mail = (isset($comment['author']['email'])) ? $comment['author']['email'] : "";
      $homepage = (isset($comment['author']['www'])) ? $comment['author']['www'] : "" ;
      $author_avatar = (isset($comment['author']['avatar'])) ? $comment['author']['avatar'] : "";
      $author_provider = (isset($comment['author']['provider'])) ? $comment['author']['provider'] : "";
      $author_anonym_name = NULL;
      $anonym_email = NULL;
    }
    else {
      $homepage = "";
      $name = $comment['anonym']['name'];
      $mail = isset($comment['anonym']['email']) ? $comment['anonym']['email'] : "";
    }
    $get_parent_local_id = 0;
    $comment_id = $comment['id'];
    if (isset($comment['parentId'])) {
      $comment_parent_id = $comment['parentId'];
      $rb = db_query("SELECT cid FROM {comment} WHERE user_agent = :user_agent", array(
        ':user_agent' => "Cackle:" . $comment_parent_id,
      ));
      foreach ($rb as $obj2) {
        $get_parent_local_id = $obj2->cid;
      }
    }
    $commentdata = array(
      'nid' => $url,
      'thread' => $url,
      'mail' => $mail,
      'homepage' => $homepage,
      'name' => $name,
      'created' => $comment['created'] / 1000,
      'hostname' => $comment['ip'],
      'subject' => '',
      'status' => $status,
      'user_agent' => 'Cackle:' . $comment['id'],
      'pid' => $get_parent_local_id,
    );
    if ($commentdata) {
      $last_id = db_insert('comment')
      ->fields($commentdata)
      ->execute();
      $this->syncMessage($comment['message'], $last_id);
      variable_set('cackle_last_comment', $comment_id);
      if ($comment['modified'] > variable_get('cackle_last_modified', NULL)) {
          variable_set('cackle_last_modified', $comment['modified']);
      }
    }
  }

  /**
   * Sync messages separately.
   */
  public function syncMessage($message, $last_id) {
    $nid = arg(1);
    $node = node_load($nid);
    $bundle = "comment_node_" . $node->type;
    $sync_message_data = array(
      'entity_type' => 'comment',
      'bundle' => $bundle,
      'entity_id' => $last_id,
      'revision_id' => $last_id,
      'delta' => 0,
      'language' => $node->language,
      'comment_body_value' => $message,
      'comment_body_format' => 'filtered_html',
    );
    db_insert('field_data_comment_body')
      ->fields($sync_message_data)
      ->execute();
  }

  function comment_status_decoder($comment) {
    $status=0;
    if (strtolower($comment['status']) == "approved") {
      $status = 1;
    }
    elseif (strtolower($comment['status'] == "pending") || strtolower($comment['status']) == "rejected") {
      $status = 0;
    }
    elseif (strtolower($comment['status']) == "spam") {
      $status = 0;
    }
    elseif (strtolower($comment['status']) == "deleted") {
      $status = 0;
    }
    return $status;
  }

  /**
   * Push comments.
   */
  public function pushComments ($response){
    $obj = $this->cackle_json_decodes($response,true);
    if ($obj) {
       // var_dump($obj);die();
      $obj = $obj['comments'];
      $comments_size = count($obj);
        if ($comments_size != 0){
          foreach ($obj as $comment) {
            if ($comment['id'] > variable_get('cackle_last_comment', NULL)) {
              $this->insertComment($comment, $this->comment_status_decoder($comment));
            } else {
                $this->update_comment_status($comment['id'], $this->comment_status_decoder($comment), $comment['modified'], $comment['message'] );
              }
          }
        }
      return $comments_size;
    }


  }


  function update_comment_status($comment_id, $status, $modified, $comment_content) {
    $status_data = array(
      'status' => $status,
    );
    $comment_data = array(
      'comment_body_value' => $comment_content,
    );
    db_update('comment')
      ->fields($status_data)
      ->condition('user_agent', "Cackle:$comment_id")
      ->execute();
    $cid = db_select('comment','cid')
      ->fields('cid')
      ->condition('user_agent', "Cackle:$comment_id")
      ->execute()
      ->fetchAssoc();

    db_update('field_data_comment_body')
      ->fields($comment_data)
      ->condition('entity_id', $cid)
      ->execute();
    variable_set('cackle_last_modified', $modified); //saving last comment id to database
    }

  /**
   * Get Local Comments.
   */
  public function getLocalComments($nodeid) {
    $get_all_comments = db_query("SELECT * FROM {comment} as t1 LEFT JOIN {field_data_comment_body} AS t2 ON (t1.cid = t2.entity_id) WHERE t1.nid = :nod_id", array(
      ':nod_id' => $nodeid,
    ));
    return $get_all_comments;
  }

  /**
   * List comments.
   */
  public function listComments($nodeid) {
    $obj = $this->getLocalComments($nodeid);
  }
}
