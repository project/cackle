Cackle - is a global comment system with the ability to login via popular 
social networks such as Google+, Facebook, Twitter, LinkedIn, Vkontakte, 
Odnoklassniki, Mail.ru and many others.
This module allows users to use social networks accounts to leave comments.

Supported social networks and OpenID providers:

Google, Google+, Facebook, Twitter, Vkontakte, Mail.ru, Odnoklassniki, Yandex, 
Rambler, LinkedIn, Yahoo, MyOpenID, Live Journal, Flickr, WordPress, Blogger, 
Verisign

Cackle for Drupal:

*Comments indexable by search engines (SEO-friendly). For that enable, please,
curl extension on your server.
*Auto-sync (backup) of comments with Cackle and WordPress database 
*Custom html for seo
*Comments counter for each post

Cackle Features:

*Unlimited sites
*Unlimited moderators
*Share on Vkontakte, Mail.ru, Facebook, Twitter, LinkedIn
*Multimedia attachments (PNG, JPG, YouTube, Vimeo, SlideShare and other...)
*Moderation comments through widget, without leaving your site
*Threaded comments and replies
*Notifications and reply by email
*Anonymous commenting
*Powerful admin tools
*Ban by IP, User
*Nasty words filter
*Customization of widget text labels
*Easy to install

Installation

Please refer to the project page for installation instructions.
